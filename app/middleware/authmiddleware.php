<?php

namespace App\Middleware;

use App\Models\User;
use App\Services\Authentication;
use Core\Exceptions\RecordNotFoundException;
use Core\Middleware;

class AuthMiddleware extends Middleware
{


    /**
     *
     */
    public function filter()
    {
        $uuid = $this->env()->request->getCookie(Authentication::UUID);
        try {
            $user = User::where('uuid', '=', $uuid)
                ->get()->first();
        } catch (RecordNotFoundException $e) {

            $request = $this->env()->request->get();
            try {
                $user = User::where('name', '=', $request['username'])
                    ->andWhere('password', '=', sha1($request['password']))
                    ->get()->first();
                $uuid = openssl_random_pseudo_bytes(Authentication::UUID_LENGTH);
                $user->uuid = $uuid;
                $user->save();
                $this->env()->response->setCookie('uuid', $uuid);
            } catch (RecordNotFoundException $e) {
                $user = null;
            }

        } finally {
            if ($this->env()->request->isPost() && $user == null) {
                $vars = array('error' => 'Вы ввели неверные данные');
                $this->env()->response->redirectWith("/anna/login", $vars);
            }
            $this->store('user', $user);
        }
    }

}