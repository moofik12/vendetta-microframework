<?php

namespace App\Middleware;

use App\Services\Authorization;
use Core\Middleware;


/*Access Control List Middleware*/

class AclMiddleware extends Middleware
{
    private $authorization;


    function __construct(Authorization $authorization)
    {
        $this->authorization = $authorization;
    }


    public function filter()
    {
        $user = $this->extract('user');
        $this->authorization->setUser($user);
    }

}