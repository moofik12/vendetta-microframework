<?php

namespace App\Middleware;

use App\Services\Authorization as Authorization;
use Core\Middleware as Middleware;

class SuperMiddleware extends Middleware
{
    public $authorization;


    public function __construct(Authorization $authorization)
    {
        $this->authorization = $authorization;
    }


    public function filter()
    {
        if (!$this->authorization->isSuper()) {
            $this->env()->response->redirect("/anna/admin");
        }
    }

}