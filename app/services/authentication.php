<?php

namespace App\Services;

use Core\Application as Application;


class Authentication extends Application
{
    const UUID = 'uuid';
    const UUID_LENGTH = '30';


    public function logout()
    {
        $this->env()->response->unsetCookie(self::UUID);
    }
}