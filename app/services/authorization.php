<?php

namespace App\Services;

use App\Models\Role;
use Core\Application as Application;
use Core\Exceptions\RecordNotFoundException;

/*Access Control List(Acl) Middleware*/

class Authorization extends Application
{
    const SUPER = 'super'; //super administrator
    const ADMIN = 'admin';
    const VISITOR = 'visitor';
    const GUEST = 'guest';

    public static $user;
    public static $role;
    public $access;


    public function setUser($user)
    {
        self::$user = $user;
        try {
            self::$role = Role::where(Role::getPrimaryKeyName(), '=', $user->role)
                ->get()->first()->role;
        } catch (RecordNotFoundException $e) {
            self::$role = self::GUEST;
        }
    }

    public function getRole()
    {
        return self::$role;
    }

    public function haveAccess()
    {
        $this->access = $this->isAdmin() || $this->isSuper() || $this->isVisitor();
        return $this->access;
    }

    public function isAdmin()
    {
        return self::$role == self::ADMIN;
    }

    public function isSuper()
    {
        return self::$role == self::SUPER;
    }

    public function isVisitor()
    {
        return self::$role == self::VISITOR;
    }

}