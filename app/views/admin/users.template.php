<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Vendetta Microframework</title>

</head>

<body class="bg-white">

@include 'app/views/admin/nav.template.php'

{{super}}
<?php if ($success): ?>
    <div style="background: green; border: 1px solid black; margin-bottom: 10px; padding: 20px;">
        <?= $success ?>
    </div>
<?php endif; ?>

<?php if ($error): ?>
    <div style="background: darkred; border: 1px solid black; margin-bottom: 10px; padding: 20px;">
        <?= $error ?>
    </div>
<?php endif; ?>

<?php foreach ($users as $user): ?>
    <div style="background: gold; border: 1px solid black; margin-bottom: 10px;">
        <div>
            Пользователь:
            <?= $user->name ?>
        </div>
        <div>
            Роль:
            <?= $user->role ?>
        </div>
        <div>
            <a href="/anna/admin/users/edit/<?= $user->user_id ?>">Редактировать</a>
            <form method="post" action="/anna/admin/users/delete/<?= $user->user_id ?>">
                <input type="submit" value="Удалить"</a>
            </form>
        </div>
    </div>
<?php endforeach; ?>
<div>
    <a href="/anna/admin/users/new">Добавить пользователя</a>
</div>
{{super}}


</body>
</html>