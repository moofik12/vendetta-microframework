<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Vendetta Microframework</title>

</head>

<body class="bg-white">

@include 'app/views/admin/nav.template.php'

{{admin}}
<form action="<?= $user->user_id ?>/save" method="post">
    <div style="background: gold; border: 1px solid black; margin-bottom: 10px;">
        <div>
            Пользователь:
            <input name="name" type="text" value="<?= $user->name ?>"/>
        </div>
        <div>
            Роль:
            <select name="role">
                <?php foreach ($roles as $role): ?>
                    <option
                        <?= ($user->role == $role->role) ? 'selected' : '' ?>
                            value="<?= $role->role_id ?>"><?= $role->role ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div>
            <input type="submit" value="Сохранить"/>
        </div>

    </div>
</form>
</body>
{{admin}}


</body>
</html>