<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Vendetta Microframework</title>

</head>

<body class="bg-white">

@include 'app/views/admin/nav.template.php'

{{visitor}}

<?php if ($success): ?>
    <div style="background: green; border: 1px solid black; margin-bottom: 10px; padding: 20px;">
        <?= $success ?>
    </div>
<?php endif; ?>

<?php if ($error): ?>
    <div style="background: darkred; border: 1px solid black; margin-bottom: 10px; padding: 20px;">
        <?= $error ?>
    </div>
<?php endif; ?>

<?php foreach ($articles as $article): ?>
    <div style="background: gold; border: 1px solid black; margin-bottom: 10px;">
        <div>
            Cтатья:
            <?= $article->title ?>
        </div>
        <div>
            Создана:
            <?= $article->author ?>
        </div>
        <div>
            Контент:
            <?= $article->content ?>
        </div>
        {{admin}}
        <div>
            <a href="/anna/admin/content/edit/<?=$article->article_id?>">Редактировать</a>
            <form method="post" action="/anna/admin/content/delete/<?= $article->article_id ?>">
                <input type="submit" value="Удалить"</a>
            </form>
        </div>
        {{admin}}
    </div>
<?php endforeach; ?>
{{visitor}}
{{admin}}
<div>
    <a href="/anna/admin/content/new">Добавить статью</a>
</div>
{{admin}}


</body>
</html>