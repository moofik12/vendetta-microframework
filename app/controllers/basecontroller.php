<?php

namespace App\Controllers;

use App\Services\Authentication as Authentication;
use App\Services\Authorization as Authorization;
use Core\Application as Application;

class BaseController extends Application
{
    private $acl;
    private $auth;


    public function __construct(Authorization $acl, Authentication $auth)
    {
        $this->acl = $acl;
        $this->auth = $auth;
    }


    public function index()
    {
        if (!$this->acl->haveAccess()) {
            $this->env()->response->redirect("/anna/login");
        }

        $this->env()->composer->setView('admin/main.template.php');
        $this->env()->composer->show();
    }


    public function login()
    {
        if ($this->acl->haveAccess()) {
            $this->env()->response->redirect("/anna/admin");
        }

        $this->env()->composer->setView('login.template.php');
        $this->env()->composer->show();
    }


    public function postLogin()
    {
        $this->env()->response->redirect('/anna/admin');
    }


    public function logout()
    {
        $this->auth->logout();
        $this->env()->response->redirect('/anna/login');
    }

}