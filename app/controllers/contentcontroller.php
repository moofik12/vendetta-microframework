<?php


namespace App\Controllers;

use App\Models\Article;
use App\Models\User;
use App\Services\Authentication as Authentication;
use App\Services\Authorization as Authorization;
use Core\Application as Application;
use Core\Exceptions\DuplicateEntryException;
use Core\Exceptions\ValueNotValidException;

class ContentController extends Application
{
    private $acl;
    private $auth;


    /**
     * ContentController constructor.
     * @param Authorization $acl
     * @param Authentication $auth
     */
    public function __construct(Authorization $acl, Authentication $auth)
    {
        $this->acl = $acl;
        $this->auth = $auth;
    }


    public function get()
    {
        $articles = Article::all();
        $vars = array('articles' => $articles);
        foreach ($articles as $article) {
            $article->author = User::where(User::getPrimaryKeyName(), '=', $article->author_id)
                                ->get()->first()->name;
        }

        $this->env()->response->with($vars);
        $this->env()->composer->setView('admin/content.template.php');
        $this->env()->composer->show();
    }

    public function edit($id)
    {
        $article = Article::where(Article::getPrimaryKeyName(), '=', $id)
            ->get()->first();

        $article->author = User::where(User::getPrimaryKeyName(), '=', $article->author_id)
            ->get()->first()->name;

        $fields = Article::getTableFields();


        $vars = array('article' => $article, 'fields'=>$fields);
        $this->env()->response->with($vars);
        $this->env()->composer->setView('admin/content/edit.template.php');
        $this->env()->composer->show();
    }

    /**
     * @param $id
     */
    public function save($id)
    {
        $request = $this->env()->request->get();
        $article = Article::where(Article::getPrimaryKeyName(), '=', $id)
            ->get()->first();

        foreach ($request as $item => $value) {
            $article->$item = $value;
        }

        try {
            $article->save();
        }
        catch (ValueNotValidException $e) {
            $vars = array('error' => 'Статья не обновлена. Проверьте правильность заполненных полей.');
            $this->env()->response->redirectWith("/anna/admin/content", $vars);
        }

        $vars = array('success' => 'Изменения были сохранены.');
        $this->env()->response->redirectWith("/anna/admin/content", $vars);
    }

    public function create()
    {
        $request = $this->env()->request->get();

        $article = Article::create();
        foreach ($request as $item => $value) {
            $article->$item = $value;
        }

        try {
            $article->save();
        }
        catch (ValueNotValidException $e) {
            $vars = array('error' => 'Статья не создана. Проверьте правильность заполненных полей.');
            $this->env()->response->redirectWith("/anna/admin/content", $vars);
        }

        $vars = array('success' => 'Новая статья была успешно добавлена.');
        $this->env()->response->redirectWith("/anna/admin/content", $vars);
    }

    public function createNew()
    {
        $fields = Article::getTableFields();
        $users = User::all();

        $vars = array('fields' => $fields, 'users'=>$users);
        $this->env()->response->with($vars);
        $this->env()->composer->setView('admin/content/create.template.php');
        $this->env()->composer->show();
    }

    public function delete($id) {
        $article = Article::where(Article::getPrimaryKeyName(), '=', $id)->get()->first();

        try {
            $article->delete();
        }
        catch (RecordNotFoundException $e) {
            $vars = array('error' => 'Пользователь не удалён, так как не был найден.');
            $this->env()->response->redirectWith("/anna/admin/content", $vars);
        }

        $vars = array('success' => 'Статья была успешно удалена.');
        $this->env()->response->redirectWith("/anna/admin/content", $vars);
    }
}