<?php


namespace App\Controllers;

use App\Models\Role as Role;
use App\Models\User as User;
use App\Services\Authentication as Authentication;
use App\Services\Authorization as Authorization;
use Core\Application as Application;
use Core\Exceptions\DuplicateEntryException;
use Core\Exceptions\RecordNotFoundException;
use Core\Exceptions\ValueNotValidException;
use Core\QueryBuilder;

class UsersController extends Application
{
    private $acl;
    private $auth;


    /**
     * UsersController constructor.
     * @param Authorization $acl
     * @param Authentication $auth
     */
    public function __construct(Authorization $acl, Authentication $auth)
    {

        $this->acl = $acl;
        $this->auth = $auth;

    }


    /**
     *
     */
    public function read()
    {
        $users = User::all();
        $allRoles = Role::all();
        $rolePk = Role::getPrimaryKeyName();

        foreach ($users as $user) {
            foreach ($allRoles as $role) {
                if ($role->$rolePk == $user->role) {
                    $user->role = $role->role;
                }
            }
        }

        $vars = array('users' => $users);

        $this->env()->response->with($vars);
        $this->env()->composer->setView('admin/users.template.php');
        $this->env()->composer->show();
    }


    /**
     * @param $id
     */
    public function edit($id)
    {
        $user = User::where(User::getPrimaryKeyName(), '=', $id)
            ->get()->first();
        $roles = Role::all();
        $rolePk = Role::getPrimaryKeyName();

        foreach ($roles as $role) {
            if ($role->$rolePk == $user->role) {
                $user->role = $role->role;
            }
        }

        $vars = array('user' => $user, 'roles' => $roles);

        $this->env()->response->with($vars);
        $this->env()->composer->setView('admin/users/edit.template.php');
        $this->env()->composer->show();
    }


    /**
     * @param $id
     */
    public function save($id)
    {

        $request = $this->env()->request->get();
        $user = User::where(User::getPrimaryKeyName(), '=', $id)
            ->get()->first();


        foreach ($request as $item => $value) {
            $user->$item = $value;
        }

        try {
            $user->save();
        }
        catch (DuplicateEntryException $e) {
            $vars = array('error' => 'Пользователь с таким именем уже существует');
            $this->env()->response->redirectWith("/anna/admin/users", $vars);
        }
        catch (ValueNotValidException $e) {
            $vars = array('error' => 'Введеное значение не валидно');
            $this->env()->response->redirectWith("/anna/admin/users", $vars);
        }

        $vars = array('success' => 'Изменения были сохранены.');
        $this->env()->response->redirectWith("/anna/admin/users", $vars);
    }

    public function create() {
        $request = $this->env()->request->get();

        $user = User::create();
        foreach ($request as $item => $value) {

            if($item == 'password') {
                $value = sha1($value);
            }

            $user->$item = $value;
        }

        try {
            $user->save();
        }
        catch (DuplicateEntryException $e) {
            $vars = array('error' => 'Пользователь с таким именем уже существует');
            $this->env()->response->redirectWith("/anna/admin/users", $vars);
        }

        $vars = array('success' => 'Новый пользователь был успешно добавлен.');
        $this->env()->response->redirectWith("/anna/admin/users", $vars);
    }

    public function createNew() {
        $fields = User::getTableFields();
        $roles = Role::all();

        if(($key = array_search(Authentication::UUID, $fields)) !== false) {
            unset($fields[$key]);
        }

        $vars = array('fields' => $fields, 'roles'=>$roles);
        $this->env()->response->with($vars);
        $this->env()->composer->setView('admin/users/create.template.php');
        $this->env()->composer->show();
    }

    public function delete($id)
    {
        $user = User::where(User::getPrimaryKeyName(), '=', $id)->get()->first();

        try {
            $user->delete();
        }
        catch (RecordNotFoundException $e) {
            $vars = array('error' => 'Пользователь не удалён, так как не был найден.');
            $this->env()->response->redirectWith("/anna/admin/users", $vars);
        }

        $vars = array('success' => 'Пользователь был успешно удален.');
        $this->env()->response->redirectWith("/anna/admin/users", $vars);
    }
}