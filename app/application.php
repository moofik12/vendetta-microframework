<?php

namespace App;

use App\Controllers\BaseController;
use App\Controllers\ContentController;
use App\Controllers\UsersController;
use App\Middleware\AclMiddleware;
use App\Middleware\AdminMiddleware;
use App\Middleware\AuthMiddleware;
use App\Middleware\SuperMiddleware;
use Core\Application as CoreApplication;

class Application extends CoreApplication
{


    /**
     * В этом методе можно регестрировать в окружении любые пользовательские классы
     * (с помощью методов окружения registerMiddleware и registerService)
     */
    public function register()
    {
        $this->env()->registerMiddleware('acl', AclMiddleware::class);
        $this->env()->registerMiddleware('auth', AuthMiddleware::class);
        $this->env()->registerMiddleware('admin', AdminMiddleware::class);
        $this->env()->registerMiddleware('super', SuperMiddleware::class);

        $this->env()->registerController('basecontroller', BaseController::class);
        $this->env()->registerController('userscontroller', UsersController::class);
        $this->env()->registerController('contentcontroller', ContentController ::class);
    }
}