<?php

namespace App;

class Config
{
    const DRIVER = 'mysql';
    const HOST = 'localhost';
    const DBNAME = 'test';
    const USERNAME = 'root';
    const PASSWORD = '';

    //данная опция в значении true указывает приложению наполнить БД таблицами по умолчанию
    const SEED = true;

    //данная опция в значении true указывает приложению использовать SEED только при первом запуске
    const AUTO_SWITCH_OFF_SEEDER = true;
}
