<?php

namespace App\Renderers;

use App\Services\Authorization as Authorization;
use Core\Application as Application;
use Core\Renderer as Renderer;

class AuthRenderer extends Application implements Renderer
{
    private $auth;


    /**
     * AuthRenderer constructor.
     * @param Authorization $auth
     */
    public function __construct(Authorization $auth)
    {
        $this->auth = $auth;
    }


    /**
     * @param $content
     * @param $view
     * @return mixed
     */
    public function render($content, $view)
    {
        if ($this->auth->isSuper()) {
            $content = preg_replace('#\{\{' . Authorization::SUPER . '\}\}(.*?)\{\{' . Authorization::SUPER . '\}\}#ius', '$1', $content);
            $content = preg_replace('#\{\{' . Authorization::ADMIN . '\}\}(.*?)\{\{' . Authorization::ADMIN . '\}\}#ius', '$1', $content);
            $content = preg_replace('#\{\{' . Authorization::VISITOR . '\}\}(.*?)\{\{' . Authorization::VISITOR . '\}\}#ius', '$1', $content);
        }
        if ($this->auth->isAdmin()) {
            $content = preg_replace('#\{\{' . Authorization::SUPER . '\}\}(.*?)\{\{' . Authorization::SUPER . '\}\}#ius', '', $content);
            $content = preg_replace('#\{\{' . Authorization::ADMIN . '\}\}(.*?)\{\{' . Authorization::ADMIN . '\}\}#ius', '$1', $content);
            $content = preg_replace('#\{\{' . Authorization::VISITOR . '\}\}(.*?)\{\{' . Authorization::VISITOR . '\}\}#ius', '$1', $content);
        } else if ($this->auth->isVisitor()) {
            $content = preg_replace('#{{' . Authorization::SUPER . '}}(.*?){{' . Authorization::SUPER . '}}#ius', '', $content);
            $content = preg_replace('#{{' . Authorization::ADMIN . '}}(.*?){{' . Authorization::ADMIN . '}}#ius', '', $content);
            $content = preg_replace('#{{' . Authorization::VISITOR . '}}(.*?){{' . Authorization::VISITOR . '}}#ius', '$1', $content);
        }
        return $content;
    }

}