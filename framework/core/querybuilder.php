<?php

namespace Core;

/*Фабрика Active Record объектов*/

use Core\Exceptions\RecordNotFoundException;

/**
 * Class QueryBuilder
 * @package Core
 * @property Model model
 * @property DB db
 */
class QueryBuilder
{
    private $rows;
    private $queryString;
    private $model;
    private $db;
    private $params = array();


    /**
     * QueryBuilder constructor.
     * @param $queryString
     * @param $model
     * @param $params
     */
    public function __construct($queryString, $model, $params)
    {
        $this->db = DB::getInstance();
        $this->model = $model;
        $this->queryString = $queryString;
        foreach ($params as $key => $param) {
            $this->params[$key] = $param;
        }
    }


    /**
     * @return $this
     */
    public function get()
    {
        $some = $this->db->query($this->queryString, $this->params);
        $this->rows = $some->fetchAll();
        return $this;
    }


    /**
     * @return Model
     * @throws RecordNotFoundException
     */
    public function first()
    {
        try {
            $class = $this->model;
            $instance = $class::instantiate($this->rows[0]);
        } catch (\Exception $e) {
            throw new RecordNotFoundException();
        }

        return $instance;
    }


    /**
     * @return mixed
     */
    public function getAllRows()
    {

        return $this->rows;
    }


    /**
     * @param $a
     * @param $operator
     * @param $b
     * @return $this
     */
    public function andWhere($a, $operator, $b)
    {
//		$b = '\'' . $b . '\'';
        $this->queryString .= ' AND ' . $a . $operator . ':' . $a;
        $this->params[$a] = $b;
        return $this;
    }


    /**
     * @param $a
     * @param $operator
     * @param $b
     * @return $this
     */
    public function orWhere($a, $operator, $b)
    {
//		$b = '\'' . $b . '\'';
        $this->queryString .= ' OR ' . $a . $operator . ':' . $a;
        $this->params[$a] = $b;
        return $this;
    }


}