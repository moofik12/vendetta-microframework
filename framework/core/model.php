<?php

namespace Core;

/*Реализация паттерна Active Record */

use Core\Exceptions\DuplicateEntryException;
use Core\Exceptions\RecordNotFoundException;
use Core\Exceptions\ValueExistsException;
use Core\Exceptions\ValueNotValidException;

/**
 * Class Model
 * @package Core
 * @property DB $db
 */
abstract class Model
{
    private $status;
    private static $db;
    private static $findStatementString;
    private static $rows;
    private $columns;
    private $fields = array();

    const CREATE = 1;
    const UPDATE = 2;

    /**
     * Model constructor.
     * @param $fields
     * @param $status
     * @throws RecordNotFoundException
     */
    private function __construct($fields, $status)
    {
        $this->columns = self::getColumns();
        foreach ($this->columns as $key => $value) {
            $this->columns[$this->columns[$key]['Field']] = $this->columns[$key];
            unset($this->columns[$key]);
        }
        $this->status = $status;
        if (empty($fields)) {
            throw new RecordNotFoundException();
        }

        $this->fields = $fields;
    }

    /**
     * @return array;
     */
    public static function all()
    {
        self::$db = DB::getInstance();
        self::$findStatementString = 'SELECT * FROM ' . static::getTable();
        self::$rows = self::$db->query(self::$findStatementString)->fetchAll();
        $result = array();
        foreach (self::$rows as $row) {
            $object = new \stdClass();
            foreach ($row as $key => $value) {
                $object->$key = $value;
            }
            $result[] = $object;
        }
        return $result;
    }

    /**
     * @return mixed
     */
    protected static function getTable()
    {
        return static::$table;
    }

    /**
     * @return mixed
     */
    public static function create()
    {
        $columns = static::getColumns();
        $fields = array();
        foreach ($columns as $column) {
            $fields[$column['Field']] = null;
        }

        return self::instantiateNew($fields);
    }

    public function delete()
    {
        if($this->status == self::CREATE) {
            throw new RecordNotFoundException();
        }
        $pk = static::getPrimaryKeyName();
        $deleteStatementString = 'DELETE FROM ' . static::getTable() . ' WHERE ' .
                                $pk . '=' . ':'.$pk;
        $fields = array(':'.$pk => $this->$pk);

        self::$db->query($deleteStatementString, $fields);
        $this->status = self::CREATE;
    }

    public static function instantiate($fields)
    {
        return new static($fields, self::UPDATE);
    }

    private static function instantiateNew($fields)
    {
        return new static($fields, self::CREATE);
    }

    private function validateField($key, $value)
    {
        if($value == null || $value == '') {
            if($this->columns[$key]['Null'] == 'NO') {
                  throw new ValueNotValidException();
            }
        }
        //TODO: доделать валидацию (добавить проверку на типы данных)
    }

    /**
     * @param $a
     * @param $operator
     * @param $b
     * @return QueryBuilder
     */
    public static function where($a, $operator, $b)
    {
        self::$db = DB::getInstance();
        $findStatementString = 'SELECT * FROM ' . static::getTable() . ' WHERE ' . $a . $operator
            . ':' . $a;
        $params = array();
        $params[$a] = $b;
        return new QueryBuilder($findStatementString, static::who(), $params);
    }

    /**
     * @return string
     */
    protected static function who()
    {
        return get_called_class();
    }

    /**
     * Обновляет или доабвляет запись в таблицу базы данных
     */
    public function save()
    {
        $fields = $this->fields;
        $values = array();
        $keys = '';
        $valueHolders = '';
        $pk = self::getPrimaryKeyName();
        unset($fields[$pk]);
        try {
            if ($this->status == self::CREATE) {
                $this->status = self::UPDATE;
                $createStatementString = 'INSERT INTO ' . static::getTable() . ' ({keys}) VALUES ({values})';

                foreach ($fields as $key => $field) {
                    $this->validateField($key, $field);
                    if ($field == null) {
                        continue;
                    }
                    $keys .= $key . ',';
                    $valueHolders .= ':' . $key . ',';
                    $values[':' . $key] = $field;
                }

                $keys = mb_substr($keys, 0, mb_strlen($keys) - 1);
                $valueHolders = mb_substr($valueHolders, 0, mb_strlen($valueHolders) - 1);
                $createStatementString = str_replace('{keys}', $keys, $createStatementString);
                $createStatementString = str_replace('{values}', $valueHolders, $createStatementString);
                self::$db->query($createStatementString, $values);
                return;
            }

            $updateStatementString = 'UPDATE ' . static::getTable() . ' SET ';


            foreach ($fields as $key => $value) {
                $this->validateField($key, $value);
                if($value == '') {
                    continue;
                }
                $updateStatementString .= $key . '=' . '\'' . $value . '\'' . ',';
            }

            $updateStatementString = mb_substr($updateStatementString, 0, mb_strlen($updateStatementString) - 1);
            $updateStatementString .= ' WHERE ' . $pk . ' = ' . $this->$pk;
            $updateStatementString .= ';';

            self::$db->query($updateStatementString, $fields);
        }
        catch (\PDOException $e) {
            if(strpos($e->getMessage(), 'Duplicate') !== false) {
                throw new DuplicateEntryException();
            }
            else {
                echo ":))";
                die($e);
            }
        }
    }

    /**
     * @return string
     */
    public static function getPrimaryKeyName()
    {
        $query = 'SHOW INDEXES FROM ' . static::getTable() . ' WHERE Key_name = \'PRIMARY\'';
        $result = self::$db->query($query)->fetch();
        $pk = $result["Column_name"];
        return $pk;
    }

    /**
     * @return string
     */
    private static function getColumns()
    {
        $query = 'SHOW COLUMNS FROM ' . static::getTable();
        $result = self::$db->query($query)->fetchAll();
        return $result;
    }

    public static function getTableFields()
    {
        $columns = self::getColumns();
        $result = array();
        $pk = static::getPrimaryKeyName();
        foreach ($columns as $column) {
            if($column['Field'] == $pk) {
                continue;
            }
            $result[] = $column['Field'];
        }

        return $result;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->fields[$name];
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->fields[$name] = $value;
    }


}
