<?php


namespace Core\Exceptions;

class DuplicateEntryException extends \Exception
{

    /**
     * ValueExistsException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        $message = 'Duplicated entry: ' . $message;
        parent::__construct($message, $code, $previous);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}