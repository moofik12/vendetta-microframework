<?php

namespace Core\Exceptions;

class EnvironmentIsLockedException extends \Exception
{

    /**
     * EnvironmentIsLockedException constructor.
     * @param string $message
     * @param \Exception|null $previous
     */
    public function __construct($message = "Environment is locked. Code: ",
                                \Exception $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}