<?php

namespace Core\Exceptions;

class RecordNotFoundException extends \Exception
{

    /**
     * RecordNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($message = "Database record not found. Code: ", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}