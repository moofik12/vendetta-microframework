<?php


namespace Core\Exceptions;

class ValueNotValidException extends \Exception
{

    /**
     * ValueExistsException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($message = "Database entry value you have passed is invalid", $code = 0, \Exception $previous = null)
    {

        parent::__construct($message, $code, $previous);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}