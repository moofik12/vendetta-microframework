<?php

namespace Core\Exceptions;

class ClassNotRegisteredException extends \Exception
{

    /**
     * ClassNotRegisteredException constructor.
     * @param string $ware
     * @param string $message
     * @param \Exception|null $previous
     */
    public function __construct($ware = '', $message = "Class is not registered. ",
                                \Exception $previous = null)
    {
        $ware .= '<br/>';
        parent::__construct($message . ': ' . $ware . ' Code:', 0, $previous);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}