<?php

namespace Core;

use Core\Exceptions\ClassNotRegisteredException;
use Core\Exceptions\EnvironmentIsLockedException;

class Router extends Application
{
    private static $getRules = array();
    private static $postRules = array();
    private static $middleware = array();
    private static $controller = array();


    /**
     * @param $rule
     * @param $controller
     * @param $method
     * @param null $middleware
     */
    public static function get($rule, $controller, $method, $middleware = null)
    {
        $rule = self::rule($rule, $controller, $method, $middleware);

        self::$getRules[$rule] = $method;
    }


    public static function post($rule, $controller, $method, $middleware = null)
    {
        $rule = self::rule($rule, $controller, $method, $middleware);

        self::$postRules[$rule] = $method;
    }


    private static function rule($rule, $controller, $method, $middleware = null)
    {
        $rule = '#^' . $rule . '$#';
        if (preg_match_all('#{(.*?)}#', $rule, $matches)) {
            $rule = preg_replace('#{.*?}#', '([^\/]?)', $rule);
            $pos = strrpos($rule, '?');
            if ($pos !== false) {
                $rule = substr_replace($rule, '', $pos, strlen('?'));
            }
        }

        if ($middleware != null) {
            if (!is_array($middleware)) {
                self::$middleware[$rule][] = $middleware;
            } else {
                foreach ($middleware as $ware) {
                    self::$middleware[$rule][] = $ware;
                }
            }
        }

        if ($controller != null) {
            self::$controller[$rule] = $controller;
        }

        return $rule;
    }



    /**
     *
     */
    public function dispatch()
    {
        if($this->env()->request->isPost()) {
            $rules = self::$postRules;
        }
        else {
            $rules = self::$getRules;
        }

        foreach ($rules as $rule => $method) {
            if (preg_match($rule, $_SERVER['REQUEST_URI'], $matches)) {
                try {
                    foreach (self::$middleware[$rule] as $key => $ware) {
                        $this->env()->launchMiddleware($ware);
                    }
                    if (!empty($matches)) {
                        array_shift($matches);
                        $controller = $this->env()->getController(self::$controller[$rule]);
                        call_user_func_array(array($controller, $method), $matches);
                    } else {
                        $controller = $this->env()->getController(self::$controller[$rule]);
                        call_user_func(array($controller, $method));
                    }
                } catch (ClassNotRegisteredException $e) {
                    print_r($e->getMessage());
                    die($e->getTraceAsString());
                } catch (EnvironmentIsLockedException $e) {
                    print_r($e->getMessage());
                    die($e->getTraceAsString());
                }
                break;
            }
        }
    }

}
