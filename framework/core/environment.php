<?php

namespace Core;

use Core\Exceptions\ClassNotRegisteredException;
use Core\Exceptions\EnvironmentIsLockedException;

/**
 * Class Environment
 * @package Core
 * @property Session session
 * @property Cookie cookie
 * @property ViewComposer composer
 * @property Response response
 * @property Request request
 */
class Environment
{
    const SERVICE = 'SERVICE';
    const CONTROLLER = 'CONTROLLER';
    const MIDDLEWARE = 'MIDDLEWARE';
    const STATUS_OPENED = 1;
    const STATUS_LOCKED = 3;
    private static $status;
    public $db;
    public $composer;
    public $router;
    private $services;
    private $middlewares;
    private $controllers;

    /**
     * Environment constructor - конструктор окружения
     * @param ViewComposer $composer
     * @param bool $seed
     */
    public function __construct($router, $composer, $seed = false)
    {
        self::tryEnvLockedException();

        $this->router = $router;
        $this->composer = $composer;
        $this->db = DB::getInstance();

        //изначальное наполнение базы данных при первичном запуске происходит с помощью метода seed
        if ($seed) {
            $this->db->seed();
        }
    }


    /*
     * Наше окружение достаточно умное для того чтобы попытаться угадать входящие аргументы
     * конструктора класса который мы пытаемся зарегестрировать как сервис
     * ( Только в случае если у класса в сигнатуре метода явно заданы типы ожидаемых аргументов )
     */

    /**
     * Пробрасывает исключение с сообщением о блокировке DI-контейнера в случае, если окружение
     * заблокировано
     * @throws EnvironmentIsLockedException
     */
    private static function tryEnvLockedException()
    {
        if (self::$status == self::STATUS_LOCKED) {
            throw new EnvironmentIsLockedException();
        }
    }

    /**
     * @param $middlewareName - имя Middleware для его регистрации в окружении
     * @param $className - $className - класс Middleware
     */
    public function registerMiddleware($middlewareName, $className)
    {
        $this->registerService($middlewareName, $className, self::MIDDLEWARE);
    }

    /**
     * @param $serviceName - имя сервиса для его регистрации в окружении
     * @param $className - класс сервиса
     * @param string $type - тип сервиса
     */
    public function registerService($serviceName, $className, $type = self::SERVICE)
    {
        self::tryEnvLockedException();

        $class = new \ReflectionClass($className);
        $params = [];
        $args = [];

        /*попытаемся угадать/инстанцировать аргументы если они требуются конструктором*/
        if ($class->getConstructor() != null) {
            $params = $class->getConstructor()->getParameters();
        }
        foreach ($params AS $param) {
            if ($param->getClass() != null) {
                $name = $param->getClass()->name;
                $args[] = new $name;
            }
        }

        $instance = $class->newInstanceArgs($args);

        if ($type == self::SERVICE) {
            $this->services[$serviceName] = $instance;
        } else if ($type == self::MIDDLEWARE) {
            $this->middlewares[$serviceName] = $instance;
        } else if ($type == self::CONTROLLER) {
            $this->controllers[$serviceName] = $instance;
        }
    }

    /**
     * @param $name - имя Middleware отправленного на выполнение
     * @throws ClassNotRegisteredException
     */
    public function launchMiddleware($name)
    {
        self::tryEnvLockedException();
        $this->lock();

        if (array_key_exists($name, $this->middlewares)) {
            $this->middlewares[$name]->filter();
            $this->unlock();
        } else {
            throw new ClassNotRegisteredException($name);
        }
    }

    /**
     * Блокирует доступ к методу выполнения Middleware и методу получения контроллера
     * Блокирует повторное инстанцирование объекта окружения
     * Блокирует регистрацию новых классов в DI-контейнере (текущее окружение - DI контейнер)
     */
    private function lock()
    {
        self::$status = self::STATUS_LOCKED;
    }

    /**
     * Разбокирует доступ к методу выполнения Middleware и методу получения контроллера
     * Разблокирует повторное инстанцирование объекта окружения
     * Разблокирует регистрацию новых классов в DI-контейнере (текущее окружение - DI контейнер)
     */
    private function unlock()
    {
        self::$status = self::STATUS_OPENED;
    }

    /**
     * @param $controllerName - имя контроллера для его регистрации в окружении
     * @param $className - класс контроллера
     */
    public function registerController($controllerName, $className)
    {
        $this->registerService($controllerName, $className, self::CONTROLLER);
    }

    /**
     * @param $name - имя контроллера для его получения из окружения
     * @return mixed - объект контроллера зарегестрированный в окружении
     * @throws ClassNotRegisteredException
     */
    public function getController($name)
    {
        self::tryEnvLockedException();
        $this->lock();

        if (array_key_exists($name, $this->controllers)) {
            return $this->controllers[$name];
        } else {
            throw new ClassNotRegisteredException($name);
        }
    }

    /**
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->services)) {
            return $this->services[$name];
        } else {
            return null;
        }
    }

}