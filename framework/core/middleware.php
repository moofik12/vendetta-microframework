<?php

namespace Core;

use Core\Exceptions\ValueExistsException;

abstract class Middleware extends Application
{
    private static $store;

    /**
     * @return mixed
     */
    abstract public function filter();


    /**
     * @param $key
     * @param $value
     * @throws ValueExistsException
     */
    public function store($key, $value)
    {
        if (!isset(self::$store[$key])) {
            self::$store[$key] = $value;
        } else {
            throw new ValueExistsException('Middleware\'s store\'s value already exists.');
        }
    }


    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return self::$store[$key];
    }


    /**
     * @param $key
     * @return mixed
     */
    public function extract($key)
    {
        $result = self::$store[$key];
        unset(self::$store[$key]);
        return $result;
    }
}