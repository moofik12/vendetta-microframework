<?php

namespace Core;

class ViewComposer extends Application
{
    const VIEWS_DIR = "app/views/";
    private $content;
    private $view;

    /**
     *
     */
    public function show()
    {
        $this->compile($this->env()->includeRenderer);
        $this->compile($this->env()->authRenderer);
        $this->compile($this->env()->varsRenderer);
        
        //TODO: сделать автоподхват пользовательских обработчиков, либо регистрацию их в отдельном хранилище

        $this->collectGarbage();
        echo $this->content;
    }

    /**
     * @param Renderer $renderer
     */
    public function compile(Renderer $renderer)
    {
        $this->content = $renderer->render($this->content, $this->view);
        file_put_contents(self::VIEWS_DIR . '/tmptpl.tpl', $this->content);
        $this->setView('tmptpl.tpl');
    }

    /**
     * @param $path
     */
    public function setView($path)
    {
        $path = self::VIEWS_DIR . $path;
        $this->view = $path;
        $this->content = file_get_contents($path);
    }

    /**
     *
     */
    public function collectGarbage()
    {
        unlink($this->view);
    }
}
