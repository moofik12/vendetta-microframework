<?php

namespace Core;

use App\Application as UserApplication;
use App\Config;
use App\Renderers\AuthRenderer as AuthRenderer;
use Renderers\IncludeRenderer as IncludeRenderer;
use Renderers\VarsRenderer as VarsRenderer;


abstract class Application
{
    const INIT = 0;
    const ENV_SET = 1;
    const READY = 2;
    protected static $env;
    private static $status = self::INIT;

    /**
     * @throws \Exception
     */
    public static function boot()
    {
        ini_set('display_errors','off');
        if (self::$status != self::INIT) {
            throw new \Exception('You can not set Environment more than one time.');
        }

        self::$status = self::ENV_SET;

        /* Создаем новое "окружение", которое будет использоваться как глобальная точка доступа к
         * сервисам, роутеру и прочим системным классам
         * во всех дочерних классах класса Application (все классы нашего приложения)*/
        $env = new Environment(new Router(), new ViewComposer(), Config::SEED);

        if(Config::SEED == true && Config::AUTO_SWITCH_OFF_SEEDER == true) {
            self::changeSeedToFalse();
        }

        /* При регистрации сервисов следующим образом нам открываются все
         * прелести Dependency Injection(IoC pattern) - мы можем в любой момент
         * сменить текущую реализацию зарегестрированных в окружении
         * классов всего лишь указав вторым параметром метода registerService
         * любой другой класс. В остальном коде приложения не придется вносить никаких правок.
         */
        $env->registerService('authRenderer', AuthRenderer::class);
        $env->registerService('includeRenderer', IncludeRenderer::class);
        $env->registerService('varsRenderer', VarsRenderer::class);
        $env->registerService('request', Request::class);
        $env->registerService('response', Response::class);
        $env->registerService('session', Session::class);
        $env->registerService('cookie', Cookie::class);
        //объект класса Auth будет доступен в окружении под именем auth , и т.д...

        /* Мы настроили окружение. Теперь задаем его в соответствие нашему "приложению".
         С этого момента во всех классах нашего приложения можно пользоваться методом env() для того
         чтобы получить доступ к окружению и запрашивать объекты базы данных,
         композитора представлений (views), а так же все сторонние сервисы (и пользовательские классы)
         которые мы зарегестрировали с помощью метода registerService() */
        Application::setEnvironment($env);

        $userApp = new UserApplication();
        $userApp->register();
    }

    private static function setEnvironment(Environment $env)
    {
        self::$env = $env;
    }

    /**
     * @throws \Exception
     */
    public static function start()
    {
        if (self::$status != self::ENV_SET) {
            throw new \Exception('You can not start() application more than one time.');
        }
        if (self::$env != null) {
            try {
                self::$env->router->dispatch();
                self::$status = self::READY;
            } catch (\Exception $e) {
                print_r($e->getMessage() . $e->getCode());
                exit();
            }
        } else {
            die('Environment is null.');
        }
    }

    /**
     * @return Environment
     */
    public function env()
    {
        return self::$env;
    }

    /**
     * Отключает опцию SEED в конфигурационном файле (меняет на false)
     * Чтобы Seeding(изначальное наполнение БД) происходило только первый раз
     * При запуске приложения
     */
    private static function changeSeedToFalse() {
        $configFile = file_get_contents('app/config.php');
        $configFile = preg_replace('#SEED[\s]+\=[\s]+true#',
            'SEED = false', $configFile);
        file_put_contents('app/config.php', $configFile);
    }
}