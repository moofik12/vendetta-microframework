<?php

namespace Core;

class Session
{
    const FLASH = 'flash';

    /**
     * Session constructor.
     */
    public function __construct()
    {
        session_start();
    }


    /*
     * @return mixed
     */
    public function getFlashData()
    {
        if (isset($_SESSION[self::FLASH])) {
            $flashData = $_SESSION[self::FLASH];
            unset($_SESSION[self::FLASH]);
            return $flashData;
        } else {
            return false;
        }
    }


    /**
     * @param $arr
     */
    public function setFlashData($arr)
    {
        foreach ($arr as $variable => $value) {
            $_SESSION[self::FLASH][$variable] = $value;
        }
    }

}