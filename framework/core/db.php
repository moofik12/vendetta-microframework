<?php

namespace Core;

use App\Config as Config;

class DB
{
    private static $instance;
    private $conn;

    private function __construct()
    {
        try {
            $this->conn = new \PDO(
                Config::DRIVER . ":host=" .
                Config::HOST . ";dbname=" .
                Config::DBNAME, Config::USERNAME, Config::PASSWORD);

            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new DB();
        }
        return self::$instance;
    }

    /**
     * Наполняет БД начальными значениями
     */
    public function seed()
    {
        $dropTables = "DROP TABLE IF EXISTS articles; DROP TABLE IF EXISTS users; DROP TABLE IF EXISTS roles;";

        $ones = sha1('111111');

        $queryroles = "CREATE TABLE roles (
					role_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
					role VARCHAR(30) NOT NULL,
					edit_users TINYINT(1) NOT NULL,
					edit_content TINYINT(1) NOT NULL,
					edit_templates TINYINT(1) NOT NULL,
					admin_approved TINYINT(1) NOT NULL,
					UNIQUE (role)
					)";

        $queryusers = "CREATE TABLE users (
					user_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
					name VARCHAR(30) NOT NULL,
					password VARCHAR(3000) NOT NULL,
					email VARCHAR(50),
				    uuid VARCHAR(50),
					role INT(6) UNSIGNED,
					UNIQUE(name),
					FOREIGN KEY (role) REFERENCES roles(role_id) ON DELETE CASCADE
					)";

        $querycontent = "CREATE TABLE articles (
                    article_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    title VARCHAR(40) NOT NULL,
                    content VARCHAR(1000) NOT NULL,
                    author_id INT(6) UNSIGNED,
                    date_of_creation DATE NOT NULL,
                    FOREIGN KEY (author_id) REFERENCES users(user_id) ON DELETE CASCADE
                    )";
        $querySeedroles = "INSERT INTO roles (role, edit_users, edit_content, edit_templates, admin_approved) VALUES ('super', 1, 1, 1, 1), ('admin', 0, 1, 1, 1), ('visitor', 0, 0, 0, 1)";

        $querySeedusers = "INSERT INTO users (name, password, email, uuid, role) VALUES 
		('super', '$ones', 'test@test.ru', null, 1), ('admin', '$ones', 'test@test.ru', null, 2), 
		('visitor', '$ones', 'test@test.ru', null, 3)";

        $querySeedcontent = "INSERT INTO articles (title, content, author_id, date_of_creation) VALUES 
		('test', '111111', 1, CURDATE())";

        if (!$this->conn->query($dropTables) === TRUE ||
            !$this->conn->query($queryroles) === TRUE ||
            !$this->conn->query($queryusers) === TRUE ||
            !$this->conn->query($querycontent) === TRUE ||
            !$this->conn->query($querySeedroles) === TRUE ||
            !$this->conn->query($querySeedusers) === TRUE ||
            !$this->conn->query($querySeedcontent) === TRUE
        ) {
            echo "Error creating table: " . $this->conn->error;
        }

    }


    /**
     * @param $query
     * @param null $values - Аргументы для биндинга к запросу
     * @return \PDOStatement
     */
    public function query($query, $values = null)
    {
        $statement = $this->conn->prepare($query);
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        if (empty($values)) {
            $res = $this->conn->query($query, \PDO::FETCH_ASSOC);
            return $res;
        }
        $result = $statement->execute($values);

        return $statement;
    }
}