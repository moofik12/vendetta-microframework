<?php

namespace Core;

class Cookie extends Application
{

    /**
     * @param $key
     * @param $value
     * @param int $time
     * @param string $path
     */
    public function set($key, $value, $time = 86400, $path = '/')
    {
        $time += time();
        setcookie($key, $value, $time, $path);
    }


    /**
     * @param $key
     * @param string $path
     */
    public function release($key, $path = '/')
    {
        setcookie($key, '', -1, $path);
    }


    /**
     * @param $key
     * @return null
     */
    public function get($key)
    {
        return isset($_COOKIE[$key]) ? $_COOKIE[$key] : null;
    }


    /**
     * @return mixed
     */
    public function all()
    {
        return $_COOKIE;
    }
}