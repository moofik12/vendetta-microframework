<?php

namespace Core;

class Response extends Application
{
    public $store = array();

    /**
     * @param $url string - URL для редиректа
     * Редирект на заданый URL
     */
    public function redirect($url)
    {
        header("Location: $url", true, 301);
        exit();
    }


    /**
     * @param $arr
     * Сохраняет переменные в сессии для передачи в шаблон, после редиректа.
     * (Эти переменные удаляются из сессии шаблонизатором)
     */
    public function redirectWith($url, $arr)
    {
        $this->env()->session->setFlashData($arr);
        header("Location: $url", true, 301);
        exit();
    }


    /**
     * @param $key
     * @param $value
     * @param int $time
     * @param string $path
     * Устанавливает куки.
     */
    public function setCookie($key, $value, $time = 86400, $path = '/')
    {
        $this->env()->cookie->set($key, $value, $time, $path);
    }


    /**
     * @param $arr
     * Сохраняет переменные во временное хранилище.
     * Используется для передачи переменных в шаблон.
     */
    public function with($arr)
    {
        foreach ($arr as $key => $value) {
            $this->store[$key] = $value;
        }
    }


    /**
     * @return array
     * Возвращает временное хранилище.
     * Используется для передачи переменных в шаблон.
     */
    public function getStore()
    {
        return $this->store;
    }


    /**
     * @param $key
     * @param string $path
     */
    public function unsetCookie($key, $path = '/')
    {
        $this->env()->cookie->release($key, $path);
    }
}