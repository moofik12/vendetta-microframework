<?php

namespace Core;

class Request extends Application
{
    private $post;
    private $get;
    private $request;


    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->post = $_POST;
        $this->get = $_GET;
        $this->request = $_REQUEST;
    }


    /**
     * @return mixed
     */
    public function get()
    {
        unset($this->request['path']);
        return $this->request;
    }


    /**
     * @param $key
     * @return null
     */
    public function getCookie($key)
    {
        return $this->env()->cookie->get($key);
    }


    /**
     * @return mixed
     */
    public function getRequestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }


    /**
     * @return bool
     */
    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }


    /**
     * @return bool
     */
    public function isGet()
    {
        return $_SERVER['REQUEST_METHOD'] == 'GET';
    }

}