<?php

namespace Core;

interface Renderer
{
    /**
     * @param $filterable
     * @param $view
     * @return mixed
     */
    public function render($filterable, $view);
}