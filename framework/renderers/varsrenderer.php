<?php

namespace Renderers;

use Core\Application as Application;
use Core\Renderer as Renderer;


class VarsRenderer extends Application implements Renderer
{
    private $view;


    /**
     * @param $content
     * @param $view
     * @return string
     */
    public function render($content, $view)
    {
        if (preg_match_all('#\$([a-zA-Z0-9]+)#ius', $content, $matches)) {
            foreach ($matches[1] as $key => $value) {
                $varArr[$value] = null;
            }
            foreach ($varArr as $key => $value) {
                $$key = null;
            }
        }

        if ($flashData = $this->env()->session->getFlashData()) {
            foreach ($flashData as $variable => $value) {
                $$variable = $value;
            }
        }

        if ($store = $this->env()->response->getStore()) {
            foreach ($store as $variable => $value) {
                $$variable = $value;
            }
        }

        ob_start();
        require_once $view;
        $content = ob_get_clean();
        return $content;
    }

}