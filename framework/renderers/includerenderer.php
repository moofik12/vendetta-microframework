<?php

namespace Renderers;

use Core\Application as Application;
use Core\Renderer as Renderer;


class IncludeRenderer extends Application implements Renderer
{


    /**
     * @param $content
     * @param $view
     * @return mixed
     */
    public function render($content, $view)
    {
        if (preg_match_all('#@include \'(.*?\.php)\'#ius', $content, $matches)) {
            if (is_array($matches[1])) {
                foreach ($matches[1] as $key => $template) {
                    $tempTpl = file_get_contents($template);
                    $content = str_replace($matches[0][$key], $tempTpl, $content);
                }
            } else {
                $tempTpl = file_get_contents($matches[1]);
                $content = str_replace($matches[0], $tempTpl, $content);
            }
        }
        return $content;
    }

}