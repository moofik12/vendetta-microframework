<?php


spl_autoload_register(function ($class_name) {
    $class_name = strtolower($class_name);
    $class_name = str_replace('\\', '/', $class_name);
    if (file_exists('framework/' . $class_name . '.php')) {
        include 'framework/' . $class_name . '.php';
    } else if (file_exists('framework/renderers/' . $class_name . '.php')) {
        include 'framework/renderers/' . $class_name . '.php';
    } else if (file_exists('app/middleware/' . $class_name . '.php')) {
        include 'app/middleware/' . $class_name . '.php';
    } else if (file_exists('app/services/' . $class_name . '.php')) {
        include 'app/services/' . $class_name . '.php';
    } else if (file_exists('framework/core/' . $class_name . '.php')) {
        include 'framework/core/' . $class_name . '.php';
    } else if (file_exists('framework/models/' . $class_name . '.php')) {
        include 'framework/models/' . $class_name . '.php';
    } else if (file_exists('framework/core/exceptions/' . $class_name . '.php')) {
        include 'framework/core/exceptions/' . $class_name . '.php';
    } else if (file_exists($class_name . '.php')) {
        include $class_name . '.php';
    }
});