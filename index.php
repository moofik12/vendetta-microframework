<?php

require 'autoloader.php';

use Core\Application as Application;
use Core\Router as Router;

/* Настройка роутинга. Формат - URL:контроллер:метод контроллера:Middlewarе, либо
 * массив Middleware */

Router::get('/anna/admin', 'basecontroller', 'index', array('auth', 'acl'));
Router::get('/anna/login', 'basecontroller', 'login', array('auth', 'acl'));
Router::get('/anna/logout', 'basecontroller', 'logout', array('auth', 'acl'));
Router::post('/anna/login', 'basecontroller', 'postLogin', array('auth', 'acl'));

Router::get('/anna/admin/users', 'userscontroller', 'read', array('auth', 'acl', 'super'));
Router::get('/anna/admin/users/new', 'userscontroller', 'createNew', array('auth', 'acl', 'super'));
Router::post('/anna/admin/users/create', 'userscontroller', 'create', array('auth', 'acl', 'super'));
Router::post('/anna/admin/users/delete/{id}', 'userscontroller', 'delete', array('auth', 'acl', 'super'));
Router::get('/anna/admin/users/edit/{id}', 'userscontroller', 'edit', array('auth', 'acl', 'super'));
Router::post('/anna/admin/users/edit/{id}/save', 'userscontroller', 'save', array('auth', 'acl', 'super'));

Router::get('/anna/admin/content', 'contentcontroller', 'get', array('auth', 'acl'));
Router::get('/anna/admin/content/new', 'contentcontroller', 'createNew', array('auth', 'acl', 'admin'));
Router::post('/anna/admin/content/create', 'contentcontroller', 'create', array('auth', 'acl', 'admin'));
Router::post('/anna/admin/content/delete/{id}', 'contentcontroller', 'delete', array('auth', 'acl', 'admin'));
Router::get('/anna/admin/content/edit/{id}', 'contentcontroller', 'edit', array('auth', 'acl'));
Router::post('/anna/admin/content/edit/{id}/save', 'contentcontroller', 'save', array('auth', 'acl', 'admin'));


/*Инициализация окружения приложения*/
Application::boot();

Application::start();